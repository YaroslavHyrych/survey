# 1.0.0 (2021-06-14)


### Features

* add build pipeline ([8a37a3b](http://bitbucket.org/YaroslavHyrych/survey/commits/8a37a3bace860a597f1d8226f91576f6723c48e1))
* add commit message linter ([16afb4b](http://bitbucket.org/YaroslavHyrych/survey/commits/16afb4bd27252648ba7fefb8b638ad37dfbe6c35))
