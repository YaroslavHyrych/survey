export interface Config {
  id: string;
  title: string;
  path: string;
  blocks: Block[];
}

export interface Block {
  id: string;
  title: string;
  fields: {
    [keyname: string]: string;
  };
}
