import { fromEvent, timer } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, finalize } from 'rxjs/operators';
import { Config, Block } from './config.interface';

const anchors: HTMLElement = document.getElementById('anchors') || ({} as any);
const form: HTMLFormElement = document.getElementById('content') || ({} as any);
const submitBtn = document.getElementById('submit') || ({} as any);
const inputFieldTmp: HTMLTemplateElement | any = document.querySelector('#input') || {};
const config: Config = {
    id: 'UBM1',
    title: 'Опитування #UBM1',
    path: '1FAIpQLSdFQKj0z-SuCHFMnOfOGrUEP1oMe0ae1QhxX3tfVPWrV56Myg',
    blocks: [
        {
            id: 'about',
            title: 'Коротко про нас',
            fields: {
                'entry.1884265043': "Ім'я",
                'entry.513669972': 'Коментар',
            },
        },
    ],
};

config.blocks.forEach(({ id, title, fields }: Block) => {
    const sectionEl: HTMLElement = document.createElement('section');
    const titleEl: HTMLHeadingElement = document.createElement('h3');
    const anchorEl: HTMLAnchorElement = document.createElement('a');
    anchorEl.textContent = titleEl.textContent = title;
    anchorEl.href = `#${id}`;
    sectionEl.id = id;
    sectionEl.appendChild(titleEl);

    Object.entries(fields).forEach(([key, value]: [string, string]) => {
        const labelEl: HTMLLabelElement | any = inputFieldTmp?.content.querySelector('label') || {};
        const inputEl: HTMLInputElement | any = inputFieldTmp?.content.querySelector('input');
        labelEl.htmlFor = key;
        labelEl.textContent = value;
        inputEl.id = inputEl.name = key;
        const inputClone = document.importNode(inputFieldTmp.content, true);
        sectionEl.appendChild(inputClone);
    });

    form.appendChild(sectionEl);
    anchors.appendChild(anchorEl);
});

timer(1000).subscribe(() => {
    document.body.classList.remove('page_loading');
});

fromEvent(submitBtn, 'click').subscribe(() => {
    const body: { [keyname: string]: string } = {
        path: config.path,
    };
    new FormData(form).forEach((value: FormDataEntryValue, key: string) => (body[key] = value));
    submitBtn.classList.add('button_processing');
    console.dir(form);
    ajax.post('http://localhost:3000/send', body)
        .pipe(finalize(unlockSubmit))
        .subscribe(
            (_) => document.body.classList.add('page_done'),
            (err) => alert(err)
        );
});

fromEvent(window, 'scroll')
    .pipe(map((v) => (window.pageYOffset > 240 ? window.pageYOffset : 240)))
    .subscribe((pos) => (anchors.style.marginTop = `${pos - 240 + 30}px`));

function unlockSubmit() {
    submitBtn.classList.remove('button_processing');
}
